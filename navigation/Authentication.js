import { Component } from 'react';
import Layout from '../components/Layout';
import { Text, Form, Input, Item, Label, Button } from 'native-base';
import * as firebase from 'firebase';
const firebaseConfig = {
  apiKey: "AIzaSyDd48_1p_PYo2nKAxu5nOQUlAi9WEqvGbo",
  authDomain: "localizador-6ea96.firebaseapp.com",
  databaseURL: "https://localizador-6ea96.firebaseio.com",
  projectId: "localizador-6ea96",
  storageBucket: "localizador-6ea96.appspot.com",
  messagingSenderId: "334061053766",
  appId: "1:334061053766:web:095b336201292b43"
};
firebase.initializeApp(firebaseConfig);

export default class Authentication extends Component {
    constructor(props){
        super(props);
        this.state = {
            email:'',
            passwd: ''
        };
        this.doAuth = this.doAuth.bind(this);
    }
    async componentDidMount(){
        firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
              console.log('usuario fez o login');
              console.log(this.props);
              
              //this.props.navigation.push('Main');
            } else {
               console.log('usuario não fez o login');
            }
          });
    }
    async doLogout(){
        firebase.auth().signOut().then(function() {
            console.log('Logout executado com sucesso!');
          }).catch(function(error) {
            console.log('Houve erro durante o logout!');
          });
    }
    async doAuth(authType){
        if (authType=='create') this.doAuthCreate();
        if (authType=='login') this.doAuthLogin();
        
    }
    async doAuthCreate(){
        firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.passwd).catch((error)=>{
            if (error) console.log(error);
            else {
                this.doAuthLogin();
            }
        });        
    }
    async doAuthLogin(){
        await firebase.auth().signInWithEmailAndPassword(this.state.email,this.state.passwd).catch((error)=>{
            if (error) console.log(error);
            else console.log('login efetuado com sucesso');
        });
        firebase.UserInfo;
        
    }
    render(){
        const { email, passwd } = this.state;
        return(
        <Layout>
            <Form>
                <Item floatingLabel>
                    <Label>Email</Label>
                    <Input value={email} onChangeText={(text)=>this.setState({email:text})} />
                </Item>
                <Item floatingLabel>
                    <Label>Senha</Label>
                    <Input value={passwd} onChangeText={(text)=>this.setState({passwd:text})} />
                </Item>
            </Form>
            <Button onPress={()=>{this.doAuth('create')}}><Text>Criar Conta</Text></Button>
            <Button onPress={()=>{this.doAuth('login')}}><Text>Fazer Login</Text></Button>
            <Button onPress={()=>{this.doLogout()}}><Text>Fazer Logout</Text></Button>
        </Layout>
        );
    }
}