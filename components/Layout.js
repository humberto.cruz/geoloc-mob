import React from 'react';
import { Container, Content, Header, Left, Right, Body, View, Text, Title } from 'native-base';

export default class Layout extends React.Component{
    render(){
        return(
            <Container>
                <View  style={{height:32, backgroundColor:'#000'}} />
                <Header>
                    <Left />
                    <Body><Title>Geolocation</Title></Body>
                    <Right />
                </Header>
                <Content style={{padding:2}}>
                    {this.props.children}
                </Content>
            </Container>
        );
    }
}